namespace AuctionAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bids",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        productId = c.String(nullable: false, maxLength: 128),
                        time = c.String(nullable: false),
                        userId = c.Int(nullable: false),
                        uniqueId = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Products", t => t.productId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.userId, cascadeDelete: true)
                .Index(t => t.productId)
                .Index(t => t.userId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        productId = c.String(nullable: false, maxLength: 128),
                        productName = c.String(),
                        productDescription = c.String(),
                        productCategory = c.String(),
                        biddingEndDate = c.String(),
                        bidHasEnded = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.productId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        firstName = c.String(nullable: false),
                        lastName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bids", "userId", "dbo.Users");
            DropForeignKey("dbo.Bids", "productId", "dbo.Products");
            DropIndex("dbo.Bids", new[] { "userId" });
            DropIndex("dbo.Bids", new[] { "productId" });
            DropTable("dbo.Users");
            DropTable("dbo.Products");
            DropTable("dbo.Bids");
        }
    }
}
