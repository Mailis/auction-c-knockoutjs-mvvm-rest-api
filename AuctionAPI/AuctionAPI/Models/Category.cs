﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionAPI.Models
{
    public class Category
    {
        public string categoryId { get; set; }       
        public IEnumerable<Link> _links { get; set; }
    }
}