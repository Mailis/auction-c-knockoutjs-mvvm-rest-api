﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AuctionAPI.Models
{
    public class Bid
    {
        public Bid(){ }

        public Bid(decimal price, string productId, string time, int userId, string uniqueId)
        {
            this.price = price;
            this.productId = productId;
            this.time = time;
            this.userId = userId;
            this.uniqueId = uniqueId;
        }

        public int id { get; set; }
        [Required]
        public decimal price { get; set; }
        // Foreign Key
        [Required]
        public string productId { get; set; }
        public Product product { get; set; }
        [Required]
        public string time { get; set; }

        // Foreign Key
        [Required]
        public int userId { get; set; }
        // Navigation property
        public User user { get; set; }
        [Required]
        public string uniqueId { get; set; }
    }
}