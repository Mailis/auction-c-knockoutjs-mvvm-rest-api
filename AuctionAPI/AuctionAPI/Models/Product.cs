﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionAPI.Models
{                     
    public class Product
    {
        public string productId { get; set; }
        public string productName { get; set; }
        public string productDescription { get; set; }
        public string productCategory { get; set; }
        public string biddingEndDate { get; set; }
        public bool bidHasEnded { get; set; }
        public IEnumerable<Link> _links { get; set; }
    }
}