﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionAPI.Models
{
    public class AuctionDTO
    {

        public int userId { get; set; }
        public string userFirstName { get; set; }
        public string userLastName { get; set; }
        public string userFullName { get; set; }
        public string uniqueId { get; set; }

        public int bidId { get; set; }     
        public decimal bidPrice { get; set; } 
        public string bidTime { get; set; }

        public string productId { get; set; }
        public string productName { get; set; }
        public string productDescription { get; set; }
        public string productCategory { get; set; }
        public string biddingEndDate { get; set; }
        public bool bidHasEnded { get; set; }
        public IEnumerable<Link> _productLinks { get; set; }
    }
}