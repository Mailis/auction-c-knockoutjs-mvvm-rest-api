﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AuctionAPI.Models;

namespace AuctionAPI.Controllers
{
    public class BidsController : ApiController
    {
        private AuctionAPIContext db = new AuctionAPIContext();

        // GET: api/Bids
        public IQueryable<AuctionDTO> GetBids()
        {
            var bids =  db.Bids.Include(b => b.user).Include(b => b.product);
            var auctions = from b in bids
                           select new AuctionDTO()
                           {
                               userId = b.userId,
                               userFullName = b.user.firstName + " " + b.user.lastName,
                               bidId = b.id,
                               bidPrice = b.price,
                               bidTime = b.time,
                               productId = b.productId,
                               productName = b.product.productName,
                               productDescription  = b.product.productDescription,
                               productCategory = b.product.productCategory,
                               biddingEndDate = b.product.biddingEndDate
                           }; 
            
            return auctions;
        }

        // GET: api/Bids/5
        [ResponseType(typeof(Bid))]
        public async Task<IHttpActionResult> GetBid(int id)
        {
            Bid bid = await db.Bids.FindAsync(id);
            if (bid == null)
            {
                return NotFound();
            }

            return Ok(bid);
        }

        private DateTime convertToUTC(String isoTime) {
            //example: isoTime = "2018-02-28T10:07:30Z";
            DateTime tt = (DateTime.Parse(isoTime)).ToUniversalTime();
            return tt;
        }

        private bool timeIsInPast(String EndDate, DateTime nowTime)
        {
            return convertToUTC(EndDate) < nowTime;
        }

        // GET: api/Bids/?finished=true
        [ResponseType(typeof(Bid))]
        public IQueryable<AuctionDTO> GetFinishedBids(bool finished)
        {
            //ZULU time of current time
            DateTime nowTime = DateTime.UtcNow.ToUniversalTime();

            IQueryable<List<Bid>> bidIDgroups = from bb in db.Bids
                                                group bb by bb.productId into productGroup
                                                select productGroup.ToList();

            //get max values
            List<Bid> maxPriceBids = new List<Bid>();
            foreach (List<Bid> bidIdGroup in bidIDgroups)
            {
                decimal maxPrice = Decimal.MinValue;
                Bid winningBid = null;
                foreach (Bid bid in bidIdGroup)
                {
                    if (bid.price > maxPrice)
                    {
                        maxPrice = bid.price;
                        winningBid = bid;
                    }
                }
                if (winningBid != null)
                {
                    maxPriceBids.Add(winningBid);
                }
            }
            //var bids = maxPriceBids.Include(p => p.product);

            var allAuctions = from b in maxPriceBids
                              join p in db.Products on b.productId equals p.productId
                              join u in db.Users on b.userId equals u.id
                              select new AuctionDTO()
                              {
                                  userId = b.userId,
                                  userFullName = b.user.firstName + " " + b.user.lastName,
                                  bidId = b.id,
                                  bidPrice = b.price,
                                  bidTime = b.time,
                                  productId = b.productId,
                                  productName = b.product.productName,
                                  productDescription = b.product.productDescription,
                                  productCategory = b.product.productCategory,
                                  biddingEndDate = b.product.biddingEndDate,
                                  uniqueId = b.uniqueId
                              };


            List<AuctionDTO> auctions = new List<AuctionDTO>();

            foreach (AuctionDTO a in allAuctions) {

                bool biddingTimeIsOver = timeIsInPast(a.biddingEndDate, nowTime);

                if (finished)
                {
                    if (biddingTimeIsOver)
                    {
                        a.bidHasEnded = true;
                        auctions.Add(a);
                    }
                }
                else
                {
                    if (!biddingTimeIsOver)
                    {
                        a.bidHasEnded = false;
                        auctions.Add(a);
                    }

                }

            }
        
                return auctions.AsQueryable();
        }

        // PUT: api/Bids/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBid(int id, Bid bid)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bid.id)
            {
                return BadRequest();
            }

            db.Entry(bid).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BidExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Bids
        [ResponseType(typeof(Bid))]
        public async Task<IHttpActionResult> PostBid(Bid bid)
        {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

            if (!sameBidExists(bid.productId, bid.userId, bid.price))
            {
                db.Bids.Add(bid);
                await db.SaveChangesAsync();
                return CreatedAtRoute("DefaultApi", new { id = bid.id }, bid);
            }

            return Ok("Bid already exists!");
        }

        // DELETE: api/Bids/5
        [ResponseType(typeof(Bid))]
        public async Task<IHttpActionResult> DeleteBid(int id)
        {
            Bid bid = await db.Bids.FindAsync(id);
            if (bid == null)
            {
                return NotFound();
            }

            db.Bids.Remove(bid);
            await db.SaveChangesAsync();

            return Ok(bid);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BidExists(int id)
        {
            return db.Bids.Count(e => e.id == id) > 0;
        }

        private bool sameBidExists(string prodId, int userId, decimal price)
        {
            return db.Bids.Count(e => (e.productId == prodId && e.userId == userId && e.price == price)) > 0;
        }
    }
}