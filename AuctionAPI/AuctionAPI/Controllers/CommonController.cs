﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AuctionAPI.Models;
using AuctionAPI.Utils;
using Newtonsoft.Json.Linq;

namespace AuctionAPI.Controllers
{
    public class CommonController : ApiController
    {

        public Product[] products_test = new Product[]
                {
            new Product { productId = "1", productName = "Tomato Soup", productCategory = "Groceries", productDescription = "new big tomato in the town", biddingEndDate = "2018-02-24T14:43:19Z" },
            new Product { productId = "2", productName = "Yo-yo", productCategory = "Toys", productDescription = "Go insane with Yo-yo palying", biddingEndDate = "2018-02-26T14:43:19Z" },
            new Product { productId = "3", productName = "Yo-yo", productCategory = "Toys", productDescription = "Go insane with Yo-yo palying", biddingEndDate = "2018-02-23T14:43:19Z" },
            new Product { productId = "4", productName = "Hammer", productCategory = "Hardware", productDescription = "Hammers all that need hammering", biddingEndDate = "2018-02-25T14:43:19Z" }
                };

        Product[] products = null;
        public Product[] getProducts()
        {
            WebClient client = new WebClient();
            client.Headers["Accept"] = "application/json";

            string returnedString = client.DownloadString(new Uri("http://uptime-auction-api.azurewebsites.net/api/Auction"));
                                                          
            this.products = Newtonsoft.Json.JsonConvert.DeserializeObject<Product[]>(returnedString);

            return this.products;
            //return products_test;
        }

    }
}
