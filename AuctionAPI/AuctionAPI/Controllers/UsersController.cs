﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AuctionAPI.Models;

namespace AuctionAPI.Controllers
{
    public class UsersController : ApiController
    {
        private AuctionAPIContext db = new AuctionAPIContext();

        // GET: api/Users
        public IQueryable<User> GetUsers()
        {
            return db.Users;
        }

        // GET: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> GetUser(int id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // GET: api/Users/5
        [HttpGet]
        //[Route("api/users/?fullname=", Name = "UserApi")]
        public IHttpActionResult GetUserId(string fullname)
        {
            string[] uName = fullname.Split('_');
            string fn = uName[0].Trim();
            string ln = uName[1].Trim();
            try
            {
                int userid = (from us in db.Users
                             where us.firstName == fn  && us.lastName == ln
                              select us.id).FirstOrDefault();
                return Ok(userid);
            }
            catch
            {
                return NotFound();
            }

        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.id)
            {
                return BadRequest();
            }

            user.lastName = user.lastName.Trim();
            user.firstName = user.firstName.Trim();

            db.Entry(user).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> PostUser(User user)
        {
            if (!UserNameExists(user))
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                user.lastName = user.lastName.Trim();
                user.firstName = user.firstName.Trim();

                db.Users.Add(user);
                await db.SaveChangesAsync();

                return CreatedAtRoute("DefaultApi", new { id = user.id }, user);
            }
            else
            {
                return Ok("User exists");
            }
        }


        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public async Task<IHttpActionResult> DeleteUser(int id)
        {
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            await db.SaveChangesAsync();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.id == id) > 0;
        }

        private bool UserNameExists(User user)
        {
            return db.Users.Count(e => e.firstName == user.firstName && e.lastName == user.lastName) > 0;
        }
    }
}