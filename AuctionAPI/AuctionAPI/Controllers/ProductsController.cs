﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AuctionAPI.Models;
using AuctionAPI.Utils;

namespace AuctionAPI.Controllers
{

    [RoutePrefix("api/products")]
    public class ProductsController : CommonController
    {

        private AuctionAPIContext db = new AuctionAPIContext();

        // GET: api/products
        [HttpGet]
        [Route("", Name = "GetAllProducts")]
        public IEnumerable<Product> GetAllProducts()
        {                                           
            Product[] products = getProducts();
            

            foreach (Product p in products) {              
                Link[] links = new Link[3];
                LinkGenerator linkGenerator = new LinkGenerator(links);
                                                                                               
                // Generate a link to a product.
                string uri_self = Url.Link("GetProductById", new { id = p.productId });              
                IEnumerable<Link> prodLinks = linkGenerator.CreateLinks(0, "GET", "self", uri_self);

                string uri_cat = Url.Link("GetProductByCategory", new { catId = p.productCategory });
                prodLinks = linkGenerator.CreateLinks(1, "GET", "categories", uri_cat);

                string uri_post_prod = Url.Link("PostAProduct", new { });
                prodLinks = linkGenerator.CreateLinks(2, "POST", "post a product", uri_post_prod);
                
                p._links = prodLinks;

                p.bidHasEnded = DateParser.bidHasEnded(p.biddingEndDate);
            }
            return products;
        }


        // GET: api/products/bidding/:biddingNotEnded
        [HttpGet]
        [Route("bidding/{isNotEnded}", Name = "GetFiltredProducts")]
        public IEnumerable<Product> GetAllFiltredProducts(bool isNotEnded = true)
        {
            Product[] products = getProducts();
            List<Product> products_filtred = new List<Product>();

            foreach (Product p in products)
            {
                bool biddingIsOver = DateParser.bidHasEnded(p.biddingEndDate);
                if (isNotEnded)
                {
                    if (!biddingIsOver)
                    {
                        Product pr = getFilteredProduct(p, biddingIsOver);
                        products_filtred.Add(pr);
                    }
                }
                else
                {
                    if (biddingIsOver)
                    {
                        Product pr = getFilteredProduct(p, biddingIsOver);
                        products_filtred.Add(pr);
                    }
                }

            }
            return products_filtred;
        }


        // GET: api/products/:id
        [HttpGet]
        [Route("{id}", Name = "GetProductById")]
        public IHttpActionResult GetProductById(string id)
        {
            Product[] products = getProducts();
            if (products == null)
            {
                return NotFound();
            }

            var product = products.FirstOrDefault((p) => p.productId == id);
            if (product == null)
            {
                return NotFound();
            }                      
  
            // Generate a link to a product. 
            Link[] links = new Link[3];
            LinkGenerator linkGenerator = new LinkGenerator(links);
                                                                    
            string uri_cat = Url.Link("GetProductByCategory", new { catId = product.productCategory });  
            IEnumerable<Link> prodLinks = linkGenerator.CreateLinks(1, "GET", "categories", uri_cat);
             //additionally, adds 2 links                                                                                    
            product._links = getGeneralLinks(prodLinks, linkGenerator);

            product.bidHasEnded = DateParser.bidHasEnded(product.biddingEndDate);
            return Ok(product);
        }


        // GET: api/products/categories
        [HttpGet]
        [Route("categories", Name = "GetAllCategories")]
        public IHttpActionResult Get()
        {
            Product[] products = getProducts();
            if (products == null)
            {
                return NotFound();
            }

            var productCats = (from p in products select p.productCategory).Distinct().ToList<string>();
            if (productCats == null)
            {
                return NotFound();
            }
                    
            Categories cats = new Categories();  
            cats.productCategories = new List<Category>(); 
                                  
            foreach (string c in productCats)
            {
                Category ca = new Category();
                ca.categoryId = c;          
                Link[] links = new Link[1];
                LinkGenerator linkGenerator = new LinkGenerator(links);
                
                string uri_cat = Url.Link("GetProductByCategory", new { catId = c });
                ca._links = linkGenerator.CreateLinks(0, "GET", ("categoriy: " + c) , uri_cat);
                cats.productCategories.Add(ca);    
            }

             //link to all products
            Link[] links2 = new Link[1];
            LinkGenerator linkGenerator2 = new LinkGenerator(links2);
            cats._links = getAllProductLinks(links2, linkGenerator2);

            return Ok(cats);
        }


                                                                                                  

        // GET: api/products/categories/:catId
        [HttpGet]
        [Route("categories/{catId}", Name = "GetProductByCategory")]
        public IHttpActionResult GetProductByCategory(string catId)
        {
            Product[] products = getProducts();
            if (products == null)
            {
                return NotFound();
            }

            var productCats =       (from p in products where p.productCategory.Equals(catId)
                                        select new Product
                                        {
                                            productId = p.productId,
                                            productCategory = p.productCategory,
                                            productDescription = p.productDescription,
                                            productName = p.productName,
                                            biddingEndDate = p.biddingEndDate
                                        }).ToList<Product>();

            if (productCats == null)
            {
                return NotFound();
            }

            foreach (Product p in productCats)
            {
                Link[] links = new Link[3];
                LinkGenerator linkGenerator = new LinkGenerator(links);       

                string uri_self = Url.Link("GetProductById", new { id = p.productId });
                IEnumerable<Link> prodLinks = linkGenerator.CreateLinks(0, "GET", "self", uri_self);
                                                                        
                //additionally, adds 2 links                           
                p._links = getGeneralLinks(prodLinks, linkGenerator);       

                p.bidHasEnded = DateParser.bidHasEnded(p.biddingEndDate);
            }                                              
            return Ok(productCats);
        }



        // POST: api/products
        [HttpPost]
        [Route("", Name = "PostAProduct")]
        [ResponseType(typeof(Product))]
        public async Task<IHttpActionResult> PostProduct(Product product)
        {
            if (!ProductExists(product.productId))
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.Products.Add(product);
                await db.SaveChangesAsync();
                return Ok(product);
            }
            else
            {
                return Ok("Product exists!");
            }

         }


        #region Helper funcitons
        private bool ProductExists(string id)
        {
            return db.Products.Count(e => e.productId == id) > 0;
        }



        /// <summary>
        /// Makes two links: for all products and for all categories
        /// </summary>
        /// <param name="prodLinks"></param>
        /// <param name="linkGenerator"></param>
        /// <returns></returns>
        protected IEnumerable<Link> getGeneralLinks(IEnumerable<Link> prodLinks, LinkGenerator linkGenerator)
        {                                                                
            prodLinks = getAllProductLinks(prodLinks, linkGenerator);     
            prodLinks = getAllProductCategoriesLinks(prodLinks, linkGenerator);
            return prodLinks;
        }


        protected IEnumerable<Link> getAllProductLinks(IEnumerable<Link> prodLinks, LinkGenerator linkGenerator)
        {                                                
            string uri_all = Url.Link("GetAllProducts", new { });
            prodLinks = linkGenerator.CreateLinks(1, "GET", "all products", uri_all);
                                                                                                  
            return prodLinks;
        }

        protected IEnumerable<Link> getAllProductCategoriesLinks(IEnumerable<Link> prodLinks, LinkGenerator linkGenerator)
        {                                         
            string uri_all_cats = Url.Link("GetAllCategories", new { });
            prodLinks = linkGenerator.CreateLinks(2, "GET", "all categories", uri_all_cats);
            return prodLinks;
        }

        /// <summary>
        /// filters product based on a knowedged wether its
        /// bidding date has expired or not
        /// </summary>
        /// <param name="p"></param>  unfiltered product
        /// <param name="biddingIsOver"></param> boolean   filter
        /// <returns></returns>
        private Product getFilteredProduct(Product p, bool biddingIsOver)
        {
            Link[] links = new Link[2];
            LinkGenerator linkGenerator = new LinkGenerator(links);

            // Generate a link to a product.
            string uri_self = Url.Link("GetProductById", new { id = p.productId });
            IEnumerable<Link> prodLinks = linkGenerator.CreateLinks(0, "GET", "self", uri_self);
            string uri_cat = Url.Link("GetProductByCategory", new { catId = p.productCategory });
            prodLinks = linkGenerator.CreateLinks(1, "GET", "categories", uri_cat);

            p._links = prodLinks;

            p.bidHasEnded = biddingIsOver;
            return p;
        }

        #endregion


    }
}
