﻿using System;
using System.Collections.Generic;
using AuctionAPI.Models;

namespace AuctionAPI.Utils
{
    public class LinkGenerator
    {
        Link[] links;
        List<Link> enumLink = new List<Link>();
        public LinkGenerator(Link[] _links)
        {
            this.links = _links;
        }

        public IEnumerable<Link> CreateLinks(int index, string method, string relation, string url)
        {
            bool isInLinks = false;

            if(enumLink != null)
                foreach (Link li in  enumLink)
                {                            
                    if (li.rel == relation && li.href == url)
                    {
                        isInLinks = true;
                    }
                }


            if (!isInLinks)
            {
                Link l = new Link
                {
                    method = method,
                    rel = relation,
                    href = url
                };
                enumLink.Add(l);
            }
            return enumLink;
        }
    }
}
