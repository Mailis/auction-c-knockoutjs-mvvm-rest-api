﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionAPI.Utils
{
    public static class EnumerableGenerator
    {

        public static IEnumerable<string> readStrings(List<string> stringArray)
        {

            List<string> validator = new List<string>();
            for (int i = 0; i < stringArray.Count(); i++)
            {
                string s = stringArray[i];
                if (validator.IndexOf(s) < 0)
                {
                    validator.Add(s);
                }
            }
            return validator.ToArray<string>();
        }
    }
}