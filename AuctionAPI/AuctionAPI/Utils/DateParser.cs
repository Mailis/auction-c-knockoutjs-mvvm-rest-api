﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuctionAPI.Utils
{
    public static class DateParser
    {

        public static bool bidHasEnded(string datetime)
        {
            DateTime myDate;
            if (!DateTime.TryParse(datetime, out myDate))
            {
                return false;
            }

            return myDate < DateTime.UtcNow;
        }


    }
}