﻿using System;
using AuctionAPI.Controllers;
using AuctionAPI.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http;

namespace AuctionAPI.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async void TestInsertingBid()
        {
            // Arrange
            BidsController bidController = new BidsController();
            // Act
            string userName = "Marfa Meri";
            string rawTime = "2018-02-27T23:10:12.251Z";
            string[] dateArray = rawTime.Split('T');
            string date = dateArray[0];
            string time = dateArray[1];
            string uniqueId = userName + "+" + date + "+" + time;

            Bid bid = new Bid(333.777M, "64748220-3d32-499c-aaa0-0ddf2e491f02", rawTime, 3, uniqueId);

            IHttpActionResult result = await  bidController.PostBid(bid);
            // Assert
            Assert.IsNotNull(result);
        }
    }
}
