﻿var ViewModel = function () {
    var self = this;
    self.categories = ko.observableArray();          
    self.bids = ko.observableArray();
    self.error = ko.observable();
    self.remainingTime = ko.observable();
    self.isCatFilterOn = ko.observable();


    var bidsUri = 'http://localhost:54284/api/bids/';
    var productsUri = 'http://localhost:54284/api/products/';
    var productsActiveUri = 'http://localhost:54284/api/products/bidding/true';
    var productsFinishedUri = 'http://localhost:54284/api/products/bidding/false';
    var categoriesUri = 'http://localhost:54284/api/products/categories/';   
    

    // Fetch the initial data.
    getAllCategories();
    getAllProducts();

    self.newBid = {
        price: ko.observable(),
        time: ko.observable(),
        productId: ko.observable(),
        userId: ko.observable()
    }
     var activeBid = {
        price: ko.observable(),
        time: ko.observable(),
        timeremains: ko.observable(),
        productId: ko.observable(),
        userId: ko.observable()
    }

    self.addBid = function (formElement) {
/*
        var bid = {                 
            price: self.newBid.price(),
            time: "2018-02-25T14:43:19Z", //new Date().getTime(),
            productId: self.newBid.productId(),
            userId: self.newBid.userId()
        };
*/
        var bid = {
            userId: 2,
                price: 999.9,
                    time: "25.02.2018 15:22:19",
                        productId: "3"
        }

        ajaxHelper(bidsUri, 'POST', bid).done(function (item) {
            self.bids.push(item);
        });
    }

    self.showOnlyilteredCategory = function (item) {
        self.isCatFilterOn(true);
        self.categories.removeAll();
        self.categories.push(item);
        getProductsByCategory(item.categoryId);
    }

    self.resetCategories = function () {
        self.isCatFilterOn(false);
        getAllCategories();
        getAllProducts();
    } 


    self.getProductDetail = function (item) {
        ajaxHelper(productsUri + item.productId, 'GET').done(function (data) {
            //console.log('getProductDetail', data);
            self.detail(data);
        });
    }

    // GET: api/products/categories/:catId  
    function getProductsByCategory(catId) {
        ajaxHelper(categoriesUri + catId, 'GET').done(function (data) {
            //console.log('getProductsByCategory', data);
            self.bids(data);
        });
    }

    function getAllCategories() {
        ajaxHelper(categoriesUri, 'GET').done(function (data) {  
            self.categories(data.productCategories);
        });
    }

                        
    function getAllProducts() {

        var receivedProducts = [];
        
        ajaxHelper(productsActiveUri, 'GET').done(function (data) {
            Object.entries(data).map(([key, value]) => {
                value["timeRemined"] = ko.computed(function () { getRemainingTime(value.biddingEndDate) });
                receivedProducts.push(value);
            });
            console.log('receivedProducts', receivedProducts);
            self.bids(receivedProducts);
        });
    }


    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

    var getRemainingTime = function (endDate_) {
        this.startDate = new Date();
        this.endDate = Date.parse(endDate_);
        var seconds = (this.endDate - this.startDate) / 1000;
        return seconds;
    }

};

ko.applyBindings(new ViewModel());