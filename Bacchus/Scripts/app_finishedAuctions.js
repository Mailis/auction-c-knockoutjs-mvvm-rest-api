﻿var ViewModel = function () {
    var self = this;
    self.categories = ko.observableArray().extend({ deferred: true });
    self.categoryFilter = ko.observable(null);
    self.error = ko.observable();
    self.finishedAuctions = ko.observableArray().extend({ deferred: true });          

    self.userFullName = ko.observable();
    var username = getCookie(usercookie);
    //console.log('GET username', username);
    self.userFullName(username);



    self.showOnlyilteredCategory = function (item) {
        self.categoryFilter(item.categoryId);
        getFinishedAuctions();
    }

    self.resetCategories = function () {
        self.categoryFilter(null);
        getFinishedAuctions();
    } 


    getFinishedAuctions();


    function getFinishedAuctions() {
        ajaxHelper(finishedAuctionsUri, 'GET').done(function (data) {  
            self.finishedAuctions.removeAll();
            self.categories.removeAll();
            //console.log(data);
            //console.log(self.categories());

            //check if category is selected
            var catFilter = self.categoryFilter();

            //make categoryObject 
            var catObj = {};
            catObj["categoryId"] = catFilter;
            if (catObj["categoryId"]) {
                self.categories.push(catObj);
            }

            // check category is selected
            if (catFilter) {//category is selected
                data.forEach(function (obj) {
                    if (catFilter === obj.productCategory) {
                        self.finishedAuctions.push(obj);
                    }
                });
            }
            //category is not selected
            else {//add all data
                self.finishedAuctions(data);
                var validationArr = [];
                data.forEach(function (obj) {
                    var catObj = {};
                    catObj["categoryId"] = obj.productCategory;
                    if (catObj["categoryId"]) {
                        if (!validationArr.includes(obj.productCategory)){
                            validationArr.push(obj.productCategory);
                            self.categories.push(catObj);
                        }
                    }
                });
            }
        });
    }


    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

};

ko.applyBindings(new ViewModel());
