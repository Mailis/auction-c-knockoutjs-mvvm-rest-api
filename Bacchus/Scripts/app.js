﻿var ViewModel = function () {
    var self = this;
    self.categories = ko.observableArray().extend({ deferred: true });          
    self.bids = ko.observableArray().extend({ deferred: true });
    self.error = ko.observable();
    self.remainingTime = ko.observable();
    self.categoryFilter = ko.observable(false);

    self.userFullName = ko.observable();
    var username = getCookie(usercookie);
    //console.log('GET username', username);
    self.userFullName(username);

    

    // Fetch the initial data.
    getAllCategories();
    getAllProducts();


    self.notify = function (item) {
        //console.log(item);
        var ticker;
        var seconds;
        var endDate = Date.parse(item.biddingEndDate);

        ticker = setInterval(function () {
            startDate = new Date();
            var duration = (endDate - startDate);
            seconds = duration / 1000;
            if (seconds < 1 && seconds > -1) {
                self.bids.remove(item);

                //if some category is selected
                var selectedCategory = self.categoryFilter();
                if (selectedCategory) {
                    updateProductsByCategory(ticker, selectedCategory);
                }
                else {
                    updateAllProducts(ticker);
                }
            }
            var timestr = getTimeString(duration);
            item.timeRemined(timestr);
        }, 1000);
    }


/*
   
*/
    self.addBid = function (item) {
        //a time of bid making
        var isoTime = new Date().toISOString()//2011-10-05T14:48:00.000Z

        //find user id, for insertion of a bid
        var bothnames = getUserString("_");
        //console.log('bothnames', bothnames);
        var userid_res = -1;
        ajaxHelper(userNameUri + bothnames, 'GET').done(function (resId) {
            userid_res = resId;
            //console.log('resId', userid_res);
            //validate usser exists
            if (userid_res < 1) {
                self.error("No such user");
                return;
            }
            //validate price is not  negative value
            var price = item.price()
            if (price < 0) {
                self.error("Price cannot be smaller than 0");
                return;
            }

            //prepare product and bid
            var product = prepareProductForInsertingBid(item);
            var bid = prepareInsertingBid(isoTime, userid_res, item);

            //post the bidded product
            //and post the bid
            //console.log('POSTED product', product);
            ajaxHelper(productsUri, 'POST', product).done(function (prodItem) {
                //console.log('POSTED product', item);
                ajaxHelper(bidsUri, 'POST', bid).done(function (bidItem) {
                    //console.log('POSTED bidItem', bidItem);

                    //same bid with the same user and price exists
                    if (typeof (bidItem) === typeof ("")) {
                        item.success(null);
                        item.exists(bidItem);
                    }
                    else {
                        item.success("Bid is posted!");
                        item.exists(null);
                    }
                    //console.log('POSTED item', item);

                });
            });


        });
        
    }


    function prepareProductForInsertingBid(item) {
        var product = {
            productName: item.productName,
            productCategory: item.productCategory,
            productId: item.productId,
            productDescription: item.productDescription,
            biddingEndDate: item.biddingEndDate
        };
        return product;
    }


    function prepareInsertingBid(isoTime, userid_res, item) {
        //make unique userID
        var uFullname = getUserString(nameDelimiter = " ");
        var uDateTimeArray = isoTime.split('T');
        var uDate = uDateTimeArray[0];
        var uTime = uDateTimeArray[1];
        var uUniqueId = uFullname + "+" + uDate + "+" + uTime;

        var bid = {
            price: item.price(),
            time: isoTime,
            productId: item.productId,
            userId: userid_res,
            uniqueId: uUniqueId
        }
        return bid;
    };


/*
        
*/

    self.showOnlyilteredCategory = function (item) {
        self.categoryFilter(item.categoryId);
        //console.log('self.categoryFilter', self.categoryFilter());
        self.categories.removeAll();
        self.categories.push(item);
        getProductsByCategory(item.categoryId);
    }

    self.resetCategories = function () {
        self.categoryFilter(null);
        getAllCategories();
        getAllProducts();
    } 


    // GET: api/products/categories/:catId  
    function getProductsByCategory(catId) {
        var receivedProducts = [];
        ajaxHelper(categoriesUri + catId, 'GET').done(function (data) {
            //console.log('getProductsByCategory', data);
            buildBidsArray(data, receivedProducts);
            self.bids(data);
        });
    }

    function getAllCategories() {
        self.categories.removeAll();
        ajaxHelper(categoriesUri, 'GET').done(function (data) {  
            self.categories(data.productCategories);
        });
    }

    function updateAllProducts(ticker) {
        getAllCategories();
        clearInterval(ticker);
        var uri = productsActiveUri;
        ajaxHelper(uri, 'GET').done(function (data) {
            buildUpdateBidsArray(data);
        });
    }

    function updateProductsByCategory(ticker, catId) {
        clearInterval(ticker);
        var receivedProducts = [];
        ajaxHelper(categoriesUri + catId, 'GET').done(function (data) {
            //console.log("CATegORY DATA", data);
            buildUpdateBidsArray(data);
        });
    }


    function getAllProducts() {
        var receivedProducts = [];
        ajaxHelper(productsActiveUri, 'GET').done(function (data) {
            buildBidsArray(data, receivedProducts);
            self.bids(receivedProducts);
        });
    }


    function buildBidsArray(data, receivedProducts) {
        data.forEach(function (obj) {
            //console.log("value obj", obj);
            setExplicitObservabels(obj);
            receivedProducts.push(obj);
            self.notify(obj);
        });
    }


    function buildUpdateBidsArray(data) {
        var bidsCopy = self.bids().slice();
        data.forEach(function (obj) {
            var objIsInArray = isObjectInArrayById(bidsCopy, obj);
            if (!objIsInArray) {
                setExplicitObservabels(obj);
                self.bids.push(obj);
                self.notify(obj);
            }
        });
    }

    function setExplicitObservabels(obj) {
        obj["timeRemined"] = ko.observable(getRemainingTime(obj.biddingEndDate));
        obj["price"] = ko.observable();
        obj["success"] = ko.observable(null);
        obj["exists"] = ko.observable(null);
    }

//HELPER FUNCTIONS


    var getRemainingTime = function (endDate_) {
        var startDate = new Date();
        var endDate = Date.parse(endDate_);
        var duration = (this.endDate - this.startDate);

        return getTimeString(duration);
    }


    function getTimeString(duration) {
        var seconds = padZero(parseInt(duration / 1000 % 60));
        var minutes = padZero(parseInt((duration / (1000 * 60)) % 60));
        var hours = padZero(parseInt((duration / (1000 * 60 * 60)) % 24));

        return hours + ":" + minutes + ":" + seconds;
    }


    function padZero(st) {
        return (st < 10) ? ((st <= 0)? "00" : ("0" + st)) : st;
    }


    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

};

ko.applyBindings(new ViewModel());
