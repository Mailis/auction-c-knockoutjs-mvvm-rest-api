﻿var usercookie = "username";



var bidsUri = 'http://localhost:54284/api/bids/';
var productsUri = 'http://localhost:54284/api/products/';
var productsActiveUri = 'http://localhost:54284/api/products/bidding/true';
var productsFinishedUri = 'http://localhost:54284/api/products/bidding/false';
var categoriesUri = 'http://localhost:54284/api/products/categories/';   

var usersUri = 'http://localhost:54284/api/users/';
var userNameUri = 'http://localhost:54284/api/users/?fullname=';
var finishedAuctionsUri = "http://localhost:54284/api/bids/?finished=true";

var nameDelimiter_ = "_";

function getUserObject() {
    var userFullName = getCookie(usercookie);
    var nameArr = userFullName.split(" ");
    var nameObj = {};
    nameObj["firstName"] = nameArr[0];
    nameObj["lastName"] = nameArr[1];
    return nameObj;
}

function getUserString(nameDelimiter = "_") {
    var userFullName = getCookie(usercookie);
    var nameArr = userFullName.split(" ");
    return (nameArr[0] + nameDelimiter + nameArr[1] );
}



function isObjectInArrayById(array, obj) {
    var objIsInArray = false;
    for (var i in array) {
        var entry = array[i];
        if (entry.productId === obj.productId) {
            objIsInArray = true;
            break;
        }
    }

    return objIsInArray;
}

/**
 *
The parameters of the function above are the name of the cookie (cname), the value of the cookie (cvalue), and the number of days until the cookie should expire (exdays).
The function sets a cookie by adding together the cookiename, the cookie value, and the expires string.
 * @param {any} cname  cookie key
 * @param {any} cvalue cookie value
 * @param {any} exdays when expires
 */
function setCookie(cname, cvalue, exdays=1) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Take the cookiename as parameter (cname).
Create a variable (name) with the text to search for (cname + "=").
Decode the cookie string, to handle cookies with special characters, e.g. '$'
Split document.cookie on semicolons into an array called ca (ca = decodedCookie.split(';')).
Loop through the ca array (i = 0; i < ca.length; i++), and read out each value c = ca[i]).
If the cookie is found (c.indexOf(name) == 0), return the value of the cookie (c.substring(name.length, c.length).

If the cookie is not found, return "".
 * @param {any} cname cookie key
 * @return type: string or int (length or empty string)
 */
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/**
 * Delete cookie
 * @param {any} cname cookie key
 */
function eraseCookie(cname) {
    document.cookie = usercookie+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}