﻿var ViewModel = function () {
    var self = this;
    self.error = ko.observable();


    self.userFullName = ko.observable();
    var username = getCookie(usercookie);
    //console.log('GET username', username);
    self.userFullName(username);


    self.newUser = {
        firstName: ko.observable(),
        lastName: ko.observable(),
        success: ko.observable(null),
        exists: ko.observable(null),
        error: ko.observable(null)
    }


    self.logout = function () {
        eraseCookie(usercookie);
        self.userFullName(null);
        self.newUser.firstName(null);
        self.newUser.lastName(null);
    }


    self.registerUser = function (formElement) {
        var user = {
            firstName: self.newUser.firstName(),
            lastName: self.newUser.lastName()
        };
        ajaxHelper(usersUri, 'POST', user).done(function (item) {
            self.userFullName(item);
            if (typeof (item) === typeof ("")) {
                self.newUser.success(null);
                self.newUser.exists(item);
                self.newUser.error(null);
            }
            else {
                self.newUser.success("User registered!");
                self.newUser.exists(null);
                self.newUser.error(null);
            }
        });
    }

    self.loginUser = function (formElement) {
        var user = {
            firstName: self.newUser.firstName(),
            lastName: self.newUser.lastName()
        };

        ajaxHelper(usersUri, 'GET', user).done(function (usersarray) {
            var uExists = false;

            for (var u in usersarray) {
                var useru = usersarray[u];
                if (useru.firstName === user.firstName && useru.lastName === user.lastName) {
                    self.newUser.firstName(useru.firstName);
                    self.newUser.lastName(useru.lastName);
                    self.userFullName({ "first": useru.firstName, "last": useru.lastName });
                    uExists = true;
                    break;
                }
            }

            if (uExists) {
                eraseCookie(usercookie);
                //var queryString = "?first=" + useru.firstName + "&last=" + useru.lastName;
                //window.location.href = ("/" + queryString);
                //console.log('self.userFullName', self.userFullName);
                var eesn =  self.newUser.firstName();
                var tagan = self.newUser.lastName();
                var eestagan = eesn + " " + tagan;
                setCookie(usercookie, eestagan);
                window.location.href = "/";
            }
            else{
                self.newUser.error("No such user in database.");
            }
        });
    }


    function ajaxHelper(uri, method, data) {
        self.error(''); // Clear error message
        return $.ajax({
            type: method,
            url: uri,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            self.error(errorThrown);
        });
    }

};

ko.applyBindings(new ViewModel());