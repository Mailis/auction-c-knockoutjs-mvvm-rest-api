﻿using System.Web.Mvc;

namespace Bacchus.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Routes of RESTful API";

            return View();
        }

        public ActionResult UserAccount()
        {
            ViewBag.Message = "Your account page.";

            return View();
        }

        public ActionResult FinAuctions()
        {
            ViewBag.Message = "Finished auctions";

            return View();
        }

        
    }
}