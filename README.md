# Auction C# KnockoutJS  MVVM  REST API


------------------------------------------

INTRODUCTION:

Auction "Bacchus", where one can make bids for variety of products. New products are dynamically added and expired auctions are removed. 
Auction items can be filtred out by clicking on a product category listed in an adjoining menu.
Expired auctions are shown at the page "Finished Auctions" (find this at main menu bar).
Possible API URLs are findable at the page "About".


------------------------------------------


USAGE:

In order to make a bid, one must first go to the page "User" 
(find this at main menu bar) and register/login as a user for Bacchus auctions.


------------------------------------------


TECHNOLOGIES:

.NET, C#, KnockoutJS, RESTful API, MVVM design pattern.



------------------------------------------

HOW TO RUN AT YOUR WINDOWS PC:

Download source, go to folder AuctionAPI, open VisualStudio by clicking on a file "AuctionAPI.sln".
There are two projects  as shown in Solution Explorer: "AuctionApi" and "Bacchus". 
"AuctionAPI" is a backend app and "Bacchus" is a frontend app.
At first, start backend "AuctionApi" and then frontend "Bacchus". 
Then "Bacchus" becomes able to send queries against "AuctionAPI".



------------------------------------------


.